<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        // ТОКЕНЫ
        $charset = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
        $max = strlen($charset);
        for($i=0; $i<50; $i++){
            $randStr='';
            for($j=0;$j<40;$j++){
                $randStr.=$charset[rand(0,$max-1)];
            }
            \App\User::create(['token' => $randStr]);

        }


        $participants=[
            'Александра Серебрякова' => 'Serebryakova.jpg',
            'Александра Синюкова' => 'sinykova.jpg',
            'Анастасия Малиновская' => 'malinovskaya.jpg',
            'Анна Соколова' => 'sokolova.jpg',
            'Валерия Сиваш' => 'sivash.jpg',
            'Дарья Прохоренко' => 'prohorenko.jpg',
            'Карина Белобородова' => 'beloborodova.jpg',
            'Катрин Маилян' => 'mailyan.jpg',
            'Мария Коломова' => 'kolomova.jpg',
            'Мария Поданева' => 'podaneva.jpg',
            'Мария Шутова' => 'shytova.jpg',
            'Мила Вавилова' => 'vavilova.jpg',
            'Наталья Тарасова' => 'tarasova.jpg',
            'Ольга Константинова' => 'konst.jpg',
            'Ольга Кулакова' => 'kylakova.jpg',
            'Светлана Полынцева' => 'polinc.jpg',
            'Татьяна Пьянкова' => 'nkova.jpg',
            'Юлия Ермолаева' => 'ermolaeva.jpg',
            'Юлия Иванова' => 'ivanova.jpg'
        ];

        foreach($participants as $k => $v){
            \App\Participant::create([
                'name' => $k,
                'path' => $v
            ]);
        }









    }


}
