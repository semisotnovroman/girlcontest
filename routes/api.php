<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/








Route::middleware(['token'])->group(function () {

    Route::post('/setLike', 'LikeController@create');
    Route::post('/resetLike', 'LikeController@delete');
    Route::get('/checkUserToken', 'UserController@checkToken');
    Route::get('/getParticipants', 'ParticipantsController@get');

});








